// Only include file once
#pragma once

enum class EDirection : short
{
	NORTH,
	SOUTH,
	WEST,
	EAST
};
