// Only include file once
#pragma once

#include <BearLibTerminal/BearLibTerminal.hpp>
#include "World.h"
#include "Player.h"

void DrawDungeon(const World& world, Player& player);
